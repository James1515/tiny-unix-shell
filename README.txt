
Information:
1. Your name and FSUID
Name: James Ortiz
FSUID: jao17

2. Design overview: A few paragraphs describing the overall structure of your code and any
important structures.

Function Parse()
This function in in charge of reading lines, and converting them into tokens.
It saves the address to argz[] and then skips all non-white spaces that may be read.

Function Execute(char **argz, char **argx, int cntr):
This function forks a child process in order to execute an important shell function 
execvp(), the status of the child is then printed to the program showing that 
the process has exited successfully. This function also displays an error if
the forking process malfunctions or an error occurs.

main()
Main is in charge of two implementations of the program, (1) reading a file,
scanning for a delimiter like ; and being able to process shell commands up
till reaching the word quit. This portion of the code also shows what line is 
being read sequentially, and also if there are too many arguments being passed to
the program itself. (2) The interactive state takes commands directly from the
prompt, and shows any errors.


3. Complete specification: Describe how you handled any ambiguities in the specification. For
example, for this project, explain how your shell will handle lines that have no commands
between semi-colons and other error/warning conditions.

For no commands between semi-colons, the program will read from the strok() function with ";"
and increment the counter that does through the amount of lines in the code.

The program is designed to check many errors:
1. Errors in files being read when there is no file, for example. (within main)
2. Errors in the forking process. within execute()
3. Or, when there is too many arguments being passed into the executable, in this case, it shows 
its current usage in main (to read from a single batchfile).


4. Known bugs or problems- 
A list of any features that you did not implement or that you know
are not working correctly: No bugs remain while attempting to create this project. All code appears to match the reference given 
in Professor Uh's directory. 



