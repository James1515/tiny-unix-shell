/************************************************************************* 
 * Date: 1/19/2019                                                       *      *
 * Author: James Anthony Ortiz                                           * 
 * Course: COP4610 - Operating Systems                                   *
 * Description: a tiny shell implementation                              *    
 * Compile: gcc -std=c99 -Wall -Wextra -Werror -o tinysh.exe shell.c     *
 *                                                                       *
 *************************************************************************/

/* ----------------------------------------------------------------- */
/* PROGRAM  tinysh.c                                                 */
/*    This program reads in an input line, parses the input line     */
/* into tokens, and uses execvp() to execute the command.            */
/* ----------------------------------------------------------------- */

//Preprocessor library:
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

//Delarations used for input:
extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;
int cntr = 0; // track insruction count
int flag = 0;    // quit flag
/* ----------------------------------------------------------------- */
/* FUNCTION  parse:                                                  */
/*    This function takes an input line and parse it into tokens.    */
/* It first replaces all white spaces with zeros until it hits a     */
/* non-white space character which indicates the beginning of an     */
/* argument.  It saves the address to argz[], and then skips all     */
/* non-white spaces which constitute the argument.                   */
/* ----------------------------------------------------------------- */

void  parse(char *linebuff, char **argz)
{
  static char* delimiter = " \n\t";
  char *token = strtok(linebuff, delimiter);
  while (token != NULL) {
    *argz++ = token;
    token = strtok(NULL, delimiter);
  }
  *argz = (char *)'\0';                 /* mark the end of argument list  */
}//End parse()        

/* ----------------------------------------------------------------- */
/* FUNCTION execute:                                                 */
/*    This function receives a commend linebuff argument list with the   */
/* first one being a file name followed by its arguments.  Then,     */
/* this function forks a child process to execute the command using  */
/* system call execvp().                                             */
/* ----------------------------------------------------------------- */
     
void  execute(char **argz, char **argx, int cntr)
{
     pid_t  pid;
     int    status;
     pid_t ch;
     
     for(int i = 0; i < cntr; i++){

       /* fork a child process    */
       if ((pid = fork()) < 0) {           
          printf("*** ERROR: forking child process failed\n");
          exit(1);
       } //end if
       
       /* for the child process:  */
       else if (pid == 0) {
          parse(argx[i], argz); 
        
          /* execute the command  */
          if(execvp(*argz, argz) < 0) {     
            printf("Exec failed on command %s\n", *argz);
            exit(1);
          } // end if
       } // end else
     } // end for loop

     
     /* for the parent:  */
     while (cntr-- != 0){
       ch = wait(&status);       /* wait for completion  */

       if (ch > 0)
          printf("PID %ld exited with status %d\n",(long)ch, status);
     } //end while  
} //end execute()

/* ----------------------------------------------------------------- */
/*                  The main program starts here                     */
/* ----------------------------------------------------------------- */
     
int main(int argc, char *argv[])
{
     int eStr = 0;
     char  linebuff[1024];             //input line 
     char  *argz[64];                  //command line argument
     char  *argx[64];
     char  *q = "quit";
     char  *ech = "echo";

	//Checks if there is one argument in argc:
     if (argc == 1){
     
       printf("prompt> ");          //Display prompt to screen    
       while (fgets(linebuff, sizeof(linebuff), stdin)) {
       /* repeat until EOF .... */
       char *word = strtok(linebuff, ";");
            cntr = 0;
	    eStr = 0;
            
            //check to see if word is empty
	    while(word != NULL){
	      eStr = 0;
	      for(unsigned int i = 0; i < strlen(word); i++){
		if( isalpha(word[i]) )
		  eStr = 1;
	      }

              char *pch = strstr(word,q);
              char *peh = strstr(word,ech);

              // If "quit": 
              if( pch && peh){
                argx[cntr] = word;
	        word = strtok(NULL, ";");
	        cntr++;
              }
              
              // quit command
              else if(pch){
                word = strtok(NULL, ";");
                flag = 1;
              }

              // all other commands except the empty string
	      else if(eStr){
	      argx[cntr] = word;
	      word = strtok(NULL, ";");
	      cntr++;
	      }

              // empty commands
	      else
		word = strtok(NULL, ";");
	    }

	    execute(argz, argx,cntr);
         
	    if (flag)                          /* is it an "exit"?     */
	      exit(0);                         /*   exit if it is      */
        
	    printf("prompt> ");       //Display prompt to screen
       }
     }

       //If there are two arguments, open file, read and process information
	   //Line by line:
     else if (argc == 2){
       FILE *fptr;
       fptr = fopen(argv[1],"r");
	 
       printf("FILE IS: %s, argv[1] is %s\n",argv[1],argv[1]);

       // no file
       if(fptr == NULL){
         fprintf(stderr,"fopen(): No such file or directory\n");
	 exit(0);
      }
	 
       while( !feof(fptr) ){
           if ( fgets(linebuff, 1024 , fptr) != NULL){
	     printf("Retrieved line of length %ld : \n+%s\n", strlen(linebuff), linebuff);
	     /* repeat until EOF .... */
	     char *word = strtok(linebuff, ";");
	     cntr = 0;
	     eStr = 0;

             //check to see if word is empty
	     while(word != NULL){
              eStr = 0;
	      for(unsigned int i = 0; i < strlen(word); i++){
		if( isalpha(word[i]) )
		  eStr = 1;
	      }
             char *pch = strstr(word,q);
             char *peh = strstr(word,ech);

             // if echo "quit" command
             if(pch && peh){
               argx[cntr] = word;
	       word = strtok(NULL, ";");
	       cntr++;
             }

             //if quit command
             else if(pch){
               word = strtok(NULL, ";");
               flag = 1;
             }

             // all other commands except empty string
	     else if(eStr){
               argx[cntr] = word;
               word = strtok(NULL, ";");
               cntr++;
	     }

             // empty string
	     else
               word = strtok(NULL, ";");
	     }

	     execute(argz, argx, cntr);
         
           }
	   
	   else
	     break;
	 }
	 fclose(fptr);
       } //end of batch mode

     //If there are too many arguments:
     else{
       printf("Usage: tinysh [batchFile]\n");
       exit(0);
	 }
     
     if (flag)                          /* is it an "exit"?     */
	      exit(0);                         /*   exit if it is      */ 
     
     return 0;
} //end main()
